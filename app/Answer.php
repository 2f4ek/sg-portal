<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'post_id', 'right', 'text', 'description'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }


}
