<?php

namespace App;

use App\Observers\CategoryObserver;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'description', 'parent_id'
    ];
    protected $dispatchesEvents = [
        'deleted' => CategoryObserver::class,
    ];
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id');
    }

    public function getParentsAttribute()
    {
        $parents = collect([]);

        $parent = $this->parent();

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent();
        }

        return $parents;
    }

}
