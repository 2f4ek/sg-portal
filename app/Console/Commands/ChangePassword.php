<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:pass {user} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Password by Id or Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->argument('user');
        $user_field = filter_var($user, FILTER_VALIDATE_EMAIL) ? 'email' : 'id';
        $password = $this->argument('password');

        $user = User::where($user_field, $user);

        $user->update(['password' => Hash::make($password)]);

    }
}
