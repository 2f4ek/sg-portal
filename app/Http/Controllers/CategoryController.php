<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Permission;
class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    function __construct()
    {
        $this->middleware('permission:'.Permission::SUPER_ADMIN, ['only' => ['create','store', 'index', 'edit', 'update', 'destroy']]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::latest()->paginate(5);
        return view('categories.index',compact('categories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Category $category)
    {
        return view('categories.show',compact('category'))
            ->with('i', (request()->input('page', 1) - 1) * 5);;
    }

    /**
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $categories = Category::pluck('name', 'id');
        return view('categories.edit',compact(['category', 'categories']));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('categories.create', compact('categories'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|max:255',
            'description' => 'required',
        ]);
        $data = $request->all();
        $category = Category::create($data);
        return redirect()->route('categories.index')
            ->with('success','Post created successfully.');

    }


    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Category $category)
    {
        request()->validate([
            'name' => 'required|max:255',
            'description' => 'required',
        ]);
        $data = $request->all();

        $category->update([
            'parent_id' => $data['parent_id'],
            'name' => $data['name'],
            'description' => $data['description']
        ]);

        return redirect()->route('categories.index')
            ->with('success','Category updated successfully.');
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {

        $category->delete();

        return redirect()->route('categories.index')
            ->with('success','Category deleted successfully');
    }

}
