<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiveSearch extends Controller
{

    /**
     * @param Request $request
     */
    public function category(Request $request)
    {

            $query = $request->get('query');
            $category = Category::where('id', 'like', '%' . $query . '%')->get();
            $ids = [];
            foreach ($category as $item) {
                $ids[] = $item->id;
                $child_cat = Category::where('parent_id', 'like', '%' . $item->id . '%')->get();
                foreach ($child_cat as $cat) {
                    $ids[] = $cat->id;
                }
            }

            if ($query > 0) {
                $data = Post::whereIn('category_id', $ids)
                    ->get();
            } else {
                $data = Post::all();
            }

        return view('ajax.categorysearch', compact('data'))->render();

    }

    /**
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function action(Request $request)
    {
        if ($request->ajax()) {
            $query = $request->get('query');
            $category = $request->get('category');
            $cat = Category::where('id', 'like', '%' . $category . '%')->get();
            $ids = [];
            foreach ($cat as $item) {
                $ids[] = $item->id;
                $child_cat = Category::where('parent_id', 'like', '%' . $item->id . '%')->get();
                foreach ($child_cat as $cat) {
                    $ids[] = $cat->id;
                }
            }
            if ($query != '' && ($category == '' || $category <= 0)) {
                $data = Post::where('name', 'like', '%' . $query . '%')
                    ->get();
            } else if ($query != '' && $category > 0) {
                $data = Post::whereIn('category_id', $ids)
                    ->where('name', 'like', '%' . $query . '%')
                    ->get();
            } else if ($query == '' && $category > 0) {
                $data = Post::whereIn('category_id', $ids)
                    ->get();
            } else {
                $data = Post::all();
            }
        }
        return view('ajax.homesearch', compact('data'))->render();
    }
}
