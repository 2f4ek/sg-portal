<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Permission;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    function __construct()
    {
        $this->middleware('permission:'.Permission::SUPER_ADMIN, ['only' => ['create','store', 'edit', 'update', 'destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        return view('posts.index',compact('posts'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        return view('posts.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|max:255',
            'test_text' => 'required',
            'text' => 'required',
            'feature_image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
        ]);

        $data = $request->all();
        $fileName = null;
        if (request()->hasFile('feature_image')) {
            $file = request()->file('feature_image');
            $fileName =  time() ."-". md5($file->hashName()). "." . $file->getClientOriginalExtension();
            $loaction = public_path('uploads/'.$fileName);
            Image::make($file)->resize(1920,1080)->save($loaction);
        }

        $post = new Post;
        $post->user_id = Auth::id();
        $post->category_id = $data['category'];
        $post->name = $data['name'];
        $post->test_text = $data['test_text'];
        $post->image = $fileName;
        $post->save();

        $answers = $data['text'];
        foreach ( $answers as $key=>$answer ) {
                Answer::create([
                    'post_id' => $post->id,
                    'right' => ( $request['answer_true_'.++$key] ) ? 1 : 0,
                    'text' => $answer,
                    'description' => $answer
                ]);
        }


        return redirect()->route('posts.index')
            ->with('success','Post created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::pluck('name', 'id');
        return view('posts.edit',compact(['post', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $answersToDelete = $post->answers;

        foreach ($answersToDelete as $answer) {
            $answer->delete();
        }

        request()->validate([
            'name' => 'required|max:255',
            'test_text' => 'required',
            'text' => 'required',
        ]);

        $data = $request->all();

        $fileName = null;

        if (request()->hasFile('feature_image')) {
            $file = request()->file('feature_image');
            $fileName =  time() ."-". md5($file->hashName()). "." . $file->getClientOriginalExtension();
            $loaction = public_path('uploads/'.$fileName);
            Image::make($file)->resize(1920,1080)->save($loaction);
        }


        $answers = $data['text'];

        $answers = $data['text'];
        foreach ( $answers as $key=>$answer ) {
            Answer::create([
                'post_id' => $post->id,
                'right' => ( $request['answer_true_'.++$key] ) ? 1 : 0,
                'text' => $answer,
                'description' => $answer
            ]);
        }

        $post->category_id = $request['category'];
        $post->name = $request['name'];
        $post->test_text = $request['test_text'];
        $post->image = $fileName;
        $post->save();

        return redirect()->route('posts.index')
            ->with('success','Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('posts.index')
            ->with('success','Post deleted successfully');
    }
}
