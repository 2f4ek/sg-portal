<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    const SUPER_ADMIN = 'super-admin';
    const SUBSCRIBER = 'subscriber';
}
