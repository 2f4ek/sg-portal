<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','category_id','name', 'test_text', 'post_id'
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

}
