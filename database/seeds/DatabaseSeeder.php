<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreateCategorySeeder::class);
        $this->call(PermissionsRolesUserSeeder::class);
        $this->call(RolesSeeder::class);
    }
}
