<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate( ['name' => App\Permission::SUPER_ADMIN])->syncPermissions(App\Permission::SUPER_ADMIN);;
        Role::firstOrCreate( ['name' => App\Permission::SUBSCRIBER])->syncPermissions(App\Permission::SUBSCRIBER);
    }
}
