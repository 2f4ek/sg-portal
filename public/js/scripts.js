/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(45);


/***/ }),

/***/ 45:
/***/ (function(module, exports) {

$(document).ready(function () {

    $('#add-answer').on('click', function () {
        var data = 0;
        $('.answers-blocks').find('.answer-block').each(function () {
            if (parseInt($(this).attr('data-block')) > data) {
                data = parseInt($(this).attr('data-block'));
                return data;
            }
        });
        $('.answers-blocks').append('<div data-block="' + (data + 1) + '" class="answer-block row block-' + (data + 1) + '"><div class="col-1  center-items"><input type="checkbox" class="checkbox" name="answer_true_' + (data + 1) + '" ></div> <div class="col"><input required type="text" class="form-control" name="text[]" placeholder="Answer"></div><div class="col-1 center-items"><button type="button" class="close" data-remove="block-' + (data + 1) + '" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>');
    });
    $('*').on('click', '.close', function () {
        var meah_id = $(this).attr('data-remove').replace(/^\D+/g, '');
        $(this).parents('.answer-block').remove();
        jQuery('.answer-block ').each(function () {
            if (parseInt($(this).attr('data-block')) > meah_id) {
                var new_data = $(this).attr('data-block') - 1;
                $(this).removeAttr('class');
                $(this).attr('data-block', new_data);
                $(this).attr('class', 'answer-block row block-' + new_data);
                $(this).find('.checkbox').removeAttr('name');
                $(this).find('.checkbox').attr('name', 'answer_true_' + new_data);
                $(this).find('.close').attr('data-remove', 'block-' + new_data);
            }
        });
    });
    $('.category').on('click', function () {
        $('#search').attr('data-cat', $(this).attr('data-category'));
        get_post_from_category($(this).attr('data-category'));
    });

    $(document).on('keyup', ' #search', function () {
        var query = $(this).val();
        var cat = $(this).attr('data-cat');
        fetch_posts_data(query, cat);
    });
});

/***/ })

/******/ });
