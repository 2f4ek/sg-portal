$(document).ready(function(){

    $('#add-answer').on('click',function () {
        let data = 0;
       $('.answers-blocks').find('.answer-block').each(function () {
               if(parseInt($(this).attr('data-block')) > data ){
                   data = parseInt($(this).attr('data-block'));
                   return data;
               }
       });
        $('.answers-blocks').append('<div data-block="'+(data+1)+'" class="answer-block row block-'+(data+1)+'"><div class="col-1  center-items"><input type="checkbox" class="checkbox" name="answer_true_'+(data+1)+'" ></div> <div class="col"><input required type="text" class="form-control" name="text[]" placeholder="Answer"></div><div class="col-1 center-items"><button type="button" class="close" data-remove="block-'+(data+1)+'" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>');
    });
    $('*').on('click', '.close', function () {
        let meah_id = $(this).attr('data-remove').replace( /^\D+/g, '');
        $(this).parents('.answer-block').remove();
        jQuery('.answer-block ').each(function () {
            if ( parseInt($(this).attr('data-block')) > meah_id ) {
                let new_data = $(this).attr('data-block')-1;
                $(this).removeAttr('class');
                $(this).attr('data-block',new_data);
                $(this).attr('class','answer-block row block-'+new_data);
                $(this).find('.checkbox').removeAttr('name');
                $(this).find('.checkbox').attr('name', 'answer_true_'+new_data);
                $(this).find('.close').attr('data-remove', 'block-'+new_data);
            }
        });

    });
    $('.category').on('click', function () {
        $('#search').attr('data-cat', $(this).attr('data-category'));
        get_post_from_category($(this).attr('data-category'));
    });

    $(document).on('keyup',' #search', function () {
        let query = $(this).val();
        let cat = $(this).attr('data-cat');
        fetch_posts_data(query,cat);
    });

});

