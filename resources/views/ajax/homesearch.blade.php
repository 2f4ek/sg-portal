@foreach($data as $post)
    <div class="card mb-5">
        <div class="card-header">
            <h3 class="block"><a href="{{ route('posts.show',$post->id) }}">{{ $post->name }}</a></h3>
            <span class="badge badge-dark inline"><a
                    href="{{route('categories.show',$post->category->id)}}">{{ $post->category->name }}</a></span>
        </div>
        <div class="card-body">
            @foreach ($post->answers as $answer)
            @if ($answer->right == 1)
                <div class="alert alert-success">{{ $answer->text }}</div>
            @else
                <div class="alert alert-light">{{ $answer->text }}</div>
            @endif
            @endforeach
        </div>
    </div>
@endforeach
