@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Posts</h2>
            </div>
            <div class="pull-right">
                @can(App\Permission::SUPER_ADMIN)
                    <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Category</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Parent</th>
            <th>Name</th>
            <th>Details</th>
        </tr>
        @foreach ($categories as $category)
            <tr>
                <td>{{ ++$i }}</td>
                @if( $category->parent )
                <td><a href="{{route('categories.show',$category->parent->id)}}">{{ $category->parent->name }}</a></td>
                @else
                    <td></td>
                @endif
                <td><a href="{{route('categories.show',$category->id)}}">{{ $category->name }}</a></td>
                <td>{{ $category->description }}</td>
            </tr>
        @endforeach
    </table>


    {!! $categories->links() !!}


@endsection
