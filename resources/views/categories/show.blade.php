@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
                @can(App\Permission::SUPER_ADMIN)
                    <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Edit</a>
                @endcan
                @csrf
                    @if( $category->id != 1 )
                @method('DELETE')
                @can(App\Permission::SUPER_ADMIN)
                    <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
                        @endif
            </form>
        </div>
    </div>
    <h1>{{$category->name}}</h1>
    @if($category->parent)
        @if($category->parent->id !== $category->id && $category->parent_id !== 0)
            <h2>Parent: <a href="{{route('categories.show', $category->parent->id)}}">{{$category->parent->name}}</a></h2>
        @endif
    @endif
    <h2>Questions:</h2>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($category->posts as $post)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $post->name }}</td>
                <td>{{ $post->test_text }}</td>
                <td>
                    <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Show</a>
                        @can(App\Permission::SUPER_ADMIN)
                            <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                        @endcan


                        @csrf
                        @method('DELETE')

                        @can(App\Permission::SUPER_ADMIN)
                            <button type="submit" class="btn btn-danger">Delete</button>
                        @endcan

                    </form>
                </td>
            </tr>
        @endforeach
    </table>




@endsection
