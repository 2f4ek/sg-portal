@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        @foreach($categories as $categorie)
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('categories.show', $categorie)}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                    {{$categorie->name}} <span class="sr-only">(current)</span>
                                </a>
                                @endforeach
                            </li>
                    </ul>
                </div>
            </nav>
            <div class="col-md-12 p-3 m-2 posts-div">
                @foreach($posts as $post)
                    <div class="card mb-5">
                        <div class="card-header">
                            <h3 class="block">{{$post->name}} </h3>
                            <span class="badge badge-dark inline">{{$post->category->name}}</span>

                            <form action="{{ route('posts.destroy',$post->id) }}" method="POST" class="left-in-block">
                                <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Show</a>
                                @can(App\Permission::SUPER_ADMIN)
                                    <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                @endcan
                                @csrf
                                @method('DELETE')
                                @can(App\Permission::SUPER_ADMIN)
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                @endcan
                            </form>


                        </div>
                        <div class="card-body">
                            <p class="lead">{{$post->test_text}}</p>
                            @foreach($post->answers as $answer)
                                @if($answer->right == 1)
                                    <div class="alert alert-success">{{ $answer->text  }}</div>
                                @else
                                    <div class="alert alert-light">{{ $answer->text  }}</div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <script>
            $(document).ready(function () {
                fetch_posts_data();
                function fetch_posts_data( query = '' )
                {
                    $.ajax({
                        url:"{{route('live_search.action')}}",
                        method: 'GET',
                        data: {query:query},
                        dataType:'json',
                        success:function (data) {
                            $('.posts-div').html(data.post_data);
                        }
                    });
                }
            });
        </script>
        @endsection

