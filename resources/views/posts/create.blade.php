@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New post</h2>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::open(array('route' => 'posts.store', 'files'=>true)) !!}
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Detail:</strong>
                        <textarea class="form-control" style="height:150px" name="test_text" placeholder="Detail"></textarea>
                    </div>
                <div class="form-group answers-blocks">
                    <div data-block="1" class="answer-block row mt-3">
                            <div class="col-1 center-items">
                                <input type="checkbox" class="checkbox" name="answer_true_1" >
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="text[]" placeholder="Answer" required>
                            </div>
                        <div class="col-1 center-items">
                            <button type="button" class="close" data-remove="block-1" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        </div>
                        <div data-block="2" class="answer-block row mt-3">
                            <div class="col-1 center-items">
                                <input type="checkbox" class="checkbox" name="answer_true_2" >
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" name="text[]" placeholder="Answer" required>
                            </div>
                            <div class="col-1 center-items">
                                <button type="button" class="close" data-remove="block-2" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                    </div>
                </div>
                <div class="text-center">
                    <button id="add-answer" type="button" class="btn btn-primary text-center" data-toggle="button" aria-pressed="false" autocomplete="off">
                        Add Answer
                    </button>
                </div>
                <div class="select-container">
                    {{ Form::select('category', $categories, $categories->pluck('name'), ['class' => 'form-control'])}}
                </div>

                <div class="form-group mt-3">
                    <label for="feature_image" class="strong">Set the image(optional)</label><br>
                    {{ Form::file('feature_image')}}
                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>


    {!! Form::close() !!}


@endsection
