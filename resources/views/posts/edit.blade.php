@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit post</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('posts.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('posts.update',$post->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $post->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="test_text" placeholder="Detail">{{ $post->test_text }}</textarea>
                </div>
            </div>
        </div>
        <div class="form-group answers-blocks">
            @foreach($post->answers as $key=>$answer)
                <div data-block="{{++$key}}" class="answer-block row block-{{$key}}">
                    <div class="col-1 center-items">
                        <input type="checkbox" class="checkbox" name="answer_true_{{$key}}"
                               @if($answer->right == 1)
                               checked
                            @endif
                        >
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" name="text[]" value="{{$answer->text}}" required>
                    </div>
                    <div class="col-1 center-items">
                        <button type="button" class="close" data-remove="block-{{$key}}" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="text-center">
            <button id="add-answer" type="button" class="btn btn-primary text-center" data-toggle="button" aria-pressed="false" autocomplete="off">
                Add Answer
            </button>
        </div>
        <div class="select-container">
            {{ Form::select('category', $categories, $categories->pluck('name'), ['class' => 'form-control'])}}
        </div>
        <div class="form-group mt-3">
            <label for="feature_image" class="strong">Set the image(optional)</label><br>
            {{ Form::file('feature_image')}}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>


@endsection
