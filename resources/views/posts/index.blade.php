@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Posts</h2>
            </div>
            <div class="pull-right">
                @can(App\Permission::SUPER_ADMIN)
                    <a class="btn btn-success" href="{{ route('posts.create') }}"> Create New Post</a>
                @endcan
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Category</th>
            <th>Name</th>
            <th>Details</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($posts as $post)
            <tr>
                <td>{{ ++$i }}</td>
                @if($post->category)

                        <td ><a href="{{route('categories.show',$post->category->id)}}">{{ $post->category->name }}</a></td>

                @endif
                <td width="200px">{{ $post->name }}</td>
                <td>{{ $post->test_text }}</td>
                <td>
                    <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Show</a>
                        @can(App\Permission::SUPER_ADMIN)
                            <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                        @endcan
                        @csrf
                        @method('DELETE')
                        @can(App\Permission::SUPER_ADMIN)
                            <button type="submit" class="btn btn-danger">Delete</button>
                        @endcan
                    </form>
                </td>
            </tr>
        @endforeach
    </table>


    {!! $posts->links() !!}


@endsection
