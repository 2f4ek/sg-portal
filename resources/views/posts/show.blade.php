@extends('layouts.app')


@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="card mb-5">
                <div class="card-header">
                    <h3 class="block text-center">{{$post->name}}</h3>
                    <span class="badge badge-dark inline f14">
                        @if($post->category)
                            <a href="{{route('categories.show',$post->category->id) }}">{{$post->category->name}}</a>
                        @endif
                    </span>
                </div>
                <div class="card-body">
                    @foreach($post->answers as $answer)
                        @if($answer->right == 1)
                            <div class="alert alert-success">{{ $answer->text  }}</div>
                        @else
                            <div class="alert alert-light">{{ $answer->text  }}</div>
                        @endif
                    @endforeach
                    @if($post->image)
                        <img src="{{asset('uploads/'.$post->image)}}" alt="post_image" class="align-right img-fluid">
                    @endif
                </div>
            </div>


        </div>
    </div>
@endsection
