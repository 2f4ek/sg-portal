@extends('layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link category" data-category="0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                               All<span class="sr-only">(current)</span>
                            </a>
                        </li>
                       @foreach($categories as $categorie)
                           @if(!$categorie->parent)
                                <li class="nav-item">
                                    <a class="nav-link category" data-category="{{$categorie->id}}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                        {{$categorie->name}} <span class="sr-only">(current)</span>
                                    </a>
                                    @if( $categorie->children )
                                        <ul>
                                        @foreach($categorie->children as $chiild)
                                            <li class="nav-item child-item">
                                                <a class="nav-link category" data-category="{{$chiild->id}}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                                    {{$chiild->name}} <span class="sr-only">(current)</span>
                                                </a>
                                                @if( $chiild->children )
                                                    <ul>
                                                    @foreach($chiild->children as $child)
                                                    <li class="nav-item child-item">
                                                        <a class="nav-link category" data-category="{{$child->id}}">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                                                            {{$child->name}} <span class="sr-only">(current)</span>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                        </ul>
                                   @endif
                                </li>
                           @endif
                    @endforeach
                    </ul>
                </div>
            </nav>
            <div class="col-md-12 p-3 m-2 posts-div">

            </div>
    </div>
    @push('scripts')
        <script type="text/javascript">
            function get_post_from_category(category_id = '')
            {
                $.ajax({
                    url:"http://sg-portal.tf/live_search/category",
                    method: 'GET',
                    data: {query:category_id},
                    success:function (data) {
                        $('.posts-div').html(data);
                    }
                });
            }
            function fetch_posts_data( query = "", cat = "" )
            {
                $.ajax({
                    url:"{{route("live_search.action")}}",
                    method: "GET",
                    data: {query:query, category:cat},
                    success:function (data) {
                        $(".posts-div").html(data);
                    }
                });
            }
            fetch_posts_data();

        </script>
    @endpush
@endsection

