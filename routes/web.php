<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/', ['as'=>'home', 'uses'=>'HomeController@index']);
Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');
Route::get('/live_search/category', 'LiveSearch@category')->name('live_search.category');
Route::resource('posts','PostController');
Route::resource('categories','CategoryController');


Route::group(['middleware' => ['auth'],'guard_name' => App\Permission::SUPER_ADMIN, ], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::get('/users/{id}/restore', 'UserController@restore')->name('user.restore');
    Route::get('/users/{id}/forcedelete', 'UserController@forcedelete')->name('user.forcedelete');
});

